$(document).on("ready", function() {
  var $listaAreasItem = $(".lista-areas-item");
  var $tableUsuario = $(".table-usuario");

  $listaAreasItem.each(function(index) {
    $(this)
      .find("input[type=checkbox]")
      .addClass("item-" + index);
    $(".lista-areas-item .item-" + index).click(function() {
      $(".table-usuario#table-" + index)
        .stop()
        .slideToggle();
    });
  });

  $tableUsuario.each(function(index) {
    $(this).attr("id", "table-" + index);
  });

  var $checkArea = $(".table-usuario input[type=checkbox]");
  $checkArea.click(function() {
    $(this)
      .parent()
      .siblings()
      .find("label")
      .addClass("font-weight-bold");
    if ($(this).is(":not(:checked)")) {
      $(this)
        .parent()
        .siblings()
        .find("label")
        .removeClass("font-weight-bold");
    }
  });

  var $selTodos = $(".sel-todos");
  $selTodos.click(function() {
    if ($(this).prop("checked") == true) {
      $(this)
        .parent()
        .parent()
        .parent()
        .find(".table")
        .find($("input[type=checkbox]"))
        .prop("checked", true);
      $(this)
        .parent()
        .parent()
        .parent()
        .find(".table")
        .find("label")
        .addClass("font-weight-bold");
    } else {
      $(this)
        .parent()
        .parent()
        .parent()
        .find(".table")
        .find($("input[type=checkbox]"))
        .prop("checked", false);
      $(this)
        .parent()
        .parent()
        .parent()
        .find(".table")
        .find("label")
        .removeClass("font-weight-bold");
    }
  });

  var $checkAvaliacao = $(".avaliacao input[type=radio]");
  $checkAvaliacao.click(function() {
    console.log("pk");
    $(this)
      .siblings("label")
      .find("span.fa")
      .addClass("text-warning")
      .removeClass("text-black-50");
    $(this)
      .parent()
      .siblings("div")
      .find("label span.fa")
      .removeClass("text-warning")
      .addClass("text-black-50");
  });

  // DataTable
  var table = $("#listagem-titulos").DataTable({
    bJQueryUI: true,
    oLanguage: {
      sProcessing: "Processando...",
      sLengthMenu: "Mostrar _MENU_ registros",
      sZeroRecords: "Não foram encontrados resultados",
      sInfo: "Mostrando de _START_ até _END_ de _TOTAL_ registros",
      sInfoEmpty: "Mostrando de 0 até 0 de 0 registros",
      sInfoFiltered: "",
      sInfoPostFix: "",
      sSearch: "Buscar:",
      sUrl: "",
      oPaginate: {
        sFirst: "Primeiro",
        sPrevious: "Anterior",
        sNext: "Seguinte",
        sLast: "Último"
      }
    }
  });
});
